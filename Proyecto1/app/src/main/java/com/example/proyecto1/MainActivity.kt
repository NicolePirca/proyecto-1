package com.example.proyecto1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebBackForwardList
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var welcomeMensaageTextView: TextView
    private lateinit var welcomeSubtitleView: TextView
    private lateinit var welcomeTextButton: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        welcomeMensaageTextView= findViewById(R.id.welcome_message)
        welcomeSubtitleView=findViewById(R.id.welcome_subtile)
        welcomeTextButton= findViewById(R.id.change_text_button)
        change_text_button.setOnClickListener{changemessageAndSubtitle()}
    }
    private fun changemessageAndSubtitle() {
        welcomeMensaageTextView.text="HELLOW UNIVERSE  !!"
        welcomeSubtitleView.text="I`m here"
    }

    //
}
